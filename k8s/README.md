# Create Sealed Secrets

## Create blog-django-superuser secret

    kubectl --namespace blog create secret generic blog-django-superuser --from-literal=django-superuser-username=<USERNAME> --from-literal=django-superuser-email=<EMAIL> --from-literal=django-superuser-password=<PASSWORD> --dry-run=client -o yaml > blog-django-superuser.yaml

## Create sealed secret from blog-django-superuser

    kubeseal --format=yaml < blog-django-superuser.yaml > blog-django-superuser-sealed.yaml

## Delete secret

    rm blog-django-superuser.yaml 

## Create blog-django-database-url k8s secret

    kubectl --namespace blog create secret generic blog-django-database-url --from-literal=django-database-url=<DATABASE_URL> --dry-run=client -o yaml > blog-django-database-url.yaml

## Create sealed secret from blog-django-superuser

    kubeseal --format=yaml < blog-django-database-url.yaml > blog-django-database-url-sealed.yaml

## Delete secret

    rm blog-django-database-url.yaml 

# Helm

## Install

    helm install blog .

## Upgrade 

    helm upgrade blog .

## Delete

    helm delete blog