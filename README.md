# blog

# Development

## Initialise Django Project

    docker-compose run django django-admin startproject <SITE_NAME>

## Initialise Django app

    docker-compose run django django-admin startapp <APP_NAME>

## Install requirements

    docker-compose run django pip install -r requirements.txt

## Make Migrations

    docker-compose run django python manage.py makemigrations

## Create a super user

    docker-compose run django python manage.py createsuperuser

## Configuration

### Environment Variables

- ALLOWED_HOSTS - host1,host2,host3
- CSRF_TRUSTED_ORIGINS - http://host1,http://host2,http://host3
- DATABASE_URL - postgres://blog:blog@postgres/blog
- DEBUG - True
- DJANGO_SUPERUSER_EMAIL foo@bar.org
- DJANGO_SUPERUSER_PASSWORD bar
- DJANGO_SUPERUSER_USERNAME foo
- DATABASE_URL - postgres://blog:blog@postgres/blog

# Dependencies

1. django The web framework used.
2. django-autoslug Automatically slugifies model fields.
3. dj-database-url provide DevOps friendly way of configuring databases.
4. psycopg2 Postgres database driver.
5. whitenoise Serve static files.