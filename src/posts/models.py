from autoslug import AutoSlugField
from django.db import models
from django.utils.translation import gettext_lazy as _

# Create your models here.


class TimeStampedModel(models.Model):
    """
    An abstract base class model that provides self-updating
    ``created`` and ``modified`` fields.
    """

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


STATUS = ((0, "Draft"), (1, "Publish"))


class Status(models.TextChoices):
    DRAFT = "DRAFT", _("Draft")
    PUBLISHED = "PUBLISHED", _("Published")


class Post(TimeStampedModel):
    title = models.CharField(max_length=255, unique=True)
    slug = slug = AutoSlugField(populate_from="title")
    content = models.TextField()
    status = models.CharField(
        max_length=255,
        choices=Status.choices,
        default=Status.DRAFT,
    )

    class Meta:
        ordering = ["-created"]

    def __str__(self):
        return self.title
