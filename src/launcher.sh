#! /bin/bash

python manage.py migrate

python manage.py createsuperuser --no-input

python manage.py collectstatic --no-input

if [[ "$DEBUG" = "True" ]]
then
  python manage.py runserver 0.0.0.0:8000
else
  gunicorn --bind 0.0.0.0:8000 blog.wsgi:application
fi
