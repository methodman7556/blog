FROM python:3.10

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

WORKDIR /opt/blog

COPY requirements.txt .

RUN pip install -r requirements.txt

WORKDIR /opt/blog/src

COPY src .

RUN chmod +x ./launcher.sh
